/*Task 6. Removing unnecessary spaces*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define ROW 80

int main()
{
	int i, k;
	int m=0;
	char cString[ROW] = { 0 };

	puts("Input your row:");

	while ((cString[m] = getchar()) != '\n')				// ������ ��������� ������ � ������
	{
		m++;
	}

	for (i = strlen(cString) - 1; i > -1; i--)	
	{
		if (cString[i] == ' ')
		 if ((i == (strlen(cString) - 2)) ||				 // ������ � ����� ������
			(cString[i - 1] == ' ') ||						// ��� ������� ������
			 (i == 0))										// ������ � ������ ������

			for (k = i; k < strlen(cString); k++)			// ������� �������� ������, ��� ����� "��������" ���� ������ �� ���� �������
				cString[k] = cString[k + 1];		
	}
			
	printf(cString);										// ����� ���������� ������
	
	return 0;


}
