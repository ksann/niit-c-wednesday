/*Task 5. Passwords*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define NUMBER_OF_PASS 10

int main()
{
	int iOrder;
	int k = 0;
	int count = 0;
	char cSymbol;
	srand(time(0)); 


	while (k<NUMBER_OF_PASS)
	{
		count++;
		iOrder = rand() % 3 + 1 ;
		
		switch (iOrder)
		{
			case 1:
				cSymbol= rand() % ('Z' - 'A' + 1) + 'A';
				break;
			case 2:
				cSymbol = rand() % ('z' - 'a' + 1) + 'a';
				break;
			case 3:
				cSymbol= rand() % ('9' - '1' + 1) + '1';

		}

		printf("%c",cSymbol);
		if (count % 10 == 0)
		{
			putchar('\n');
			k++;
		}
		

	}
	return 0;


}
