/*Lab03. Task10. �������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������ */

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

int main()
{
    char str[80], ch;
    int i = 0, k = 0, count = 0, dcount = 0, lcount = 0;
    int inWord = 0, wordNum = 0;

    puts("Enter a line:");
    fgets(str, 80, stdin);
    if (str[strlen(str) - 1] == '\n')
        str[strlen(str) - 1] = ' ';

    while (1)
    {
        i = 0, k = 0, count = 0, dcount = 0, lcount = 0, inWord = 0, wordNum = 0;
        puts("Enter a number");
        if (scanf("%d", &wordNum) == 1)
        {
            while (str[i]) //���� ������� ������ �� ����� ����
            {
                if (str[i] != ' ' && inWord == 0) //������ � �����
                {
                    count++;
                    lcount++;
                    inWord = 1;
                }

                else if (str[i] != ' ' && inWord == 1) // ��� � ����� 
                {
                    lcount++;
                }

                else if (str[i] == ' ' && inWord == 1) //������� �� �����
                {
                    inWord = 0;
                    if (count == wordNum)
                    {
                        for (k = (i - lcount); k < (strlen(str) - lcount + 1); k++)			// ������� �������� �����, ������ ������ �� ��� �������� ������, 
                            str[k] = str[k + lcount];                                      //��� ����� "��������" ���� ������ ���-�� ������� ������ ����� �����
                        printf(str);
                        putchar('\n');
                        break;
                    }
                    lcount = 0;
                }
                i++;
            }
            break;
        }
        else
        {
            puts("Input error!");
            do
                ch = getchar();
            while (ch != '\n'&& ch != EOF);
        }
    }
    return 0;
}
