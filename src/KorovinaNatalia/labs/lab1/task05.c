/*Task 5. Aligning line in the middle */

#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h> 
#include <string.h> 
#define LINELENGTH 80

int main()

{
	char cString[80]; 
	char test[5];
	int SpaceCounter;

	puts("Enter your string, please (80 symbols max):");
	fgets(cString, LINELENGTH, stdin);

	if (cString[strlen(cString) - 1] == '\n') 
		cString[strlen(cString) - 1] = 0;

	SpaceCounter = (LINELENGTH - strlen(cString)) / 2 + 1 ; //���������� ������ ���� ������
	sprintf(test, "%%%ds", SpaceCounter);	
	printf(test, cString);		
	putchar('\n');
	return 0;
}
