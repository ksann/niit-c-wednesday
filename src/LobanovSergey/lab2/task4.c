#include <stdio.h>
#include <string.h>
#define STRING "00O0Th1S1s1nTereSt1nG5a5TasK"

int main()
{
	char str[] = STRING;
	int i;			        // for loop go forward
	int j;	      		        // for loop go back
	int tmp;			// for sharing

	for (i = 0, j = strlen(str)-1; i < j; i++, j--)
	{
		if (str[i] >= 'A' && str[i] <= 'Z' || str[i] >= 'a' && str[i] <= 'z')
			j++;            // don't subtract j	
		else if (str[i] >= '0' && str[i] <= '9')
		{
			if (str[j] >= 'A' && str[j] <= 'Z' || str[j] >= 'a' && str[j] <= 'z'){
				tmp = str[i];			// swap
				str[i] = str[j];
				str[j] = tmp;
			}
			else
				i--;      // don`t add i
		}
	}
	for (i = 0; i < strlen(str); i++)
		printf("%c", str[i]);
	putchar('\n');
	printf("Length of start str  %d == %d  Length finish str\n", strlen(STRING), strlen(str));
	return 0;
}