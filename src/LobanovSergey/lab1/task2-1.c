#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#define SIZE 30

int main()
{
	char name[30];
	char hello[][15] = { "Good morning", "Good afternoon", "Good evening", "Good night" };
	char mes[] = { "Enter, please" };
	int hour = 0, min = 0, sec = 0, ch, choice;

	puts("Hello! What is your name?\n");
	printf("%s:\n", mes);
	fgets(name, SIZE, stdin);
	name[strlen(name) - 1] = '\0';

	while (1)
	{
		printf("Thank you, %s!\nWhat is time now?\n%s in format (hour:min:sek):\n", name, mes);
		if ((scanf("%d:%d:%d", &hour, &min, &sec) == 3) && (hour >= 0 && hour <= 24) && (min >= 0 && min <= 60) && (sec >= 0 && sec <= 60))
		{
			// create condition 
			if (hour > 6 && hour <= 11)
			{
				choice = 0;
				break;
			}
			else if (hour >= 12 && hour <= 17)
			{
				choice = 1;
				break;
			}
			else if (hour >= 18 && hour <= 21)
			{
				choice = 2;
				break;
			}
			else if ((hour >= 22 && hour <= 24) || (hour >= 0 && hour <= 6))
			{
				choice = 0;
				break;
			}
		}
		else
			printf("Input Error! %s again:", mes);
		do
			ch = getchar();
		while (ch != '\n' && ch != EOF);
	}
	printf("%s, %s. Now is %d:%d:%d.\n", hello[choice], name, hour, min, sec);
	return 0;
}

��� ���������� �� break?