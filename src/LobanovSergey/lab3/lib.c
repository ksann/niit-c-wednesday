#include <stdio.h>

void zero(char *arr)
{
    if (arr[strlen(arr) - 1] == '\n')
        arr[strlen(arr) - 1] = 0;
    if (arr[strlen(arr) - 1] == ' ')
        arr[strlen(arr) - 1] = 0;
    return;
}

// clean buffer
void buffer()
{
    char ch;
    do
        ch = getchar();
    while (ch != '\n' && ch != EOF);
}
